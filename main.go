package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gocolly/colly/v2"
	swaggerFiles "github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"

	"gitlab.com/erwinnekketsu/golang-scrapping.git/docs"
)

// Course stores information about a coursera course
type Course struct {
	Title string
}

func handlerIndex(ctx *gin.Context) {

	urlResult := []string{
		"https://www.result.si/projekti",
		"https://www.result.si/o-nas",
		"https://www.result.si/kariera",
		"https://www.result.si/blog",
	}
	// Instantiate default collector
	c := colly.NewCollector(
		// Visit only domains: result.si, www.result.si
		colly.AllowedDomains("result.si", "www.result.si"),
	)

	// Create another collector to scrape course details
	detailCollector := c.Clone()

	courses := make([]Course, 0, 200)

	// Before making a request print "Visiting ..."
	c.OnRequest(func(r *colly.Request) {
		log.Println("visiting", r.URL.String())
	})

	// On every <a> element with et_pb_module_header class call callback
	c.OnHTML(`.et_pb_module_header`, func(e *colly.HTMLElement) {
		// Activate detailCollector if the link contains "result.si/projekti-in-resitve"
		// courseURL := e.Request.AbsoluteURL(e.Attr("href"))
		// log.Println("courseURL", courseURL)
		for i := 0; i < len(urlResult); i++ {
			// if strings.Index(urlResult[i], urlResult[0]) != 0 {
			detailCollector.Visit(urlResult[i])
			// }
		}
	})

	// Extract details of the course
	detailCollector.OnHTML(`div[class=header-content]`, func(e *colly.HTMLElement) {
		log.Println("content found", e.Request.URL)
		title := e.ChildText(".et_pb_module_header")
		if title == "" {
			log.Println("No title found", e.Request.URL)
		}
		course := Course{
			Title: title,
		}
		courses = append(courses, course)
	})

	// Start scraping on http://coursera.com/projekti/
	for i := 0; i < len(urlResult); i++ {

		c.Visit(urlResult[i])
	}

	// jsonInBytes, err := json.Marshal(courses)
	// if err != nil {
	// 	ctx.JSON(500, http.StatusInternalServerError)
	// 	// http.Error(ctx, err.Error(), http.StatusInternalServerError)
	// 	return
	// }

	ctx.JSON(200, courses)
}

func main() {
	// programmatically set swagger info
	docs.SwaggerInfo.Title = "Swagger Example API"
	docs.SwaggerInfo.Description = "This is a sample server Petstore server."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = "petstore.swagger.io"
	docs.SwaggerInfo.BasePath = "/v2"
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	r := gin.New()

	// use ginSwagger middleware to serve the API docs
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	r.GET("/", handlerIndex)
	r.GET("/index", handlerIndex)
	r.Run()

	var address = "localhost:9000"
	fmt.Printf("server started at %s\n", address)
	err := http.ListenAndServe(address, nil)
	if err != nil {
		fmt.Println(err.Error())
	}
}
