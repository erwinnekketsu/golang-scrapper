module gitlab.com/erwinnekketsu/golang-scrapping.git

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.7.1 // indirect
	github.com/gin-gonic/gin v1.7.4
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/gocolly/colly/v2 v2.1.0
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/gin-swagger v1.3.1
	github.com/swaggo/swag v1.7.1
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.5 // indirect
)
